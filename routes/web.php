<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin'], function () {

    Auth::routes(['register' => false]);
});
//
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Auth::routes(['register' => false]);

Route::get('/dashboard', 'App\Http\Controllers\HomeController@index')->name('dashboard')->middleware('auth');


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
    Route::resource('Invited', 'App\Http\Controllers\InvitedController');
    Route::resource('clients', 'App\Http\Controllers\ClientController');

    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
});

Route::prefix(LaravelLocalization::setLocale())->middleware([
    'localeSessionRedirect', 'localizationRedirect', 'localeViewPath'])
    ->group(function () {

    Route::get('/', [App\Http\Controllers\ClientController::class, 'register'])->name('home');

    Route::post('save_data', [App\Http\Controllers\ClientController::class, 'save_data'])->name('save_data');

    });
