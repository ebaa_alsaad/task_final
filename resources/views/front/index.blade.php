@extends('layouts.front.master')
@section('content')
    <div id="loading" class="row justify-content-center align-items-center align-content-center">
        <div class="row justify-content-center align-items-center align-content-center">
            <img width="10%" alt="loading-gif" src="{{asset('img/loading.gif')}}">
        </div>
    </div>
    <header id="site-header" role="banner" class="container-fluid d-flex justify-content-between header fixed-top">
        <div class="d-none d-custom-block">

        </div>
    </header>
    <div class="m-top">
        <div class="row p-m-l d-flex justify-content-center">
            <div class="col-md-4 col-12">
                <h2 class="heading-top py-3 pl-5" style="color: #ffffff">Task</h2>
            </div>
            <div class="col-md-7 col-12 center-btn" style="margin: auto 0;">
            </div>
        </div>
        <div class="row d-flex justify-content-center">

            <div class="col-xl-10 col-lg-10 col-md-7">
                <form id="main_form" action="{{route("save_data")}}" method="POST">
                    @csrf
                    <div class="card b-0 mt-3" style="border: none ; background: none">
                        @include('front.partials.step1')
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('footer-scripts')
    @include('front.partials.script')
@endpush

