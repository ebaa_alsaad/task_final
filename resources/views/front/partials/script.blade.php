<script>

    $(document).ready(function () {
        $("#submit_btn").on('click',function (event){
            event.preventDefault();
            $(".text-danger").empty();
            $("#main_form").submit();
        })
        $("#main_form").submit(function (event) {
            event.preventDefault();
            let formData = $(this).serialize();
            $.ajax({
                type: "POST",
                url: "{{route('save_data')}}",
                data: formData,
                success: function (data) {
                    new window.swal('success', data.message, 'success');
                },
                error:function (XMLHttpRequest, textStatus ,thrownError){
                    let message = XMLHttpRequest.responseJSON.message;
                    let errors = XMLHttpRequest.responseJSON.errors;
                    if(message){
                        new window.swal('error', message, 'error');
                    }
                    if(errors){
                        $.each(errors , function (key,value){
                            $(`input[name="${key}"]`).parent().find('.text-danger').append(value)
                        })
                    }
                }
            })

        });
    });
</script>
