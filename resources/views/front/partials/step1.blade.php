<fieldset class="show">
    <div class="form-card validate_form">
        <div class="row">
            <div class="col-md-6"><h3 class="font-weight-normal">{{trans('labels.front.What is your data?')}}</h3></div>
            <div class="col-md-6"><h6 class="text-danger mb-3 text-right">* {{trans('labels.front.Obligated')}}</h6></div>
        </div>
        <div class="row">
            <div class="col-12"><h5 class=" mb-4" style="color: #08999e;">{{trans('labels.front.fill in the fields')}}</h5></div>
        </div>

        <div class="row form-group">
            <div class="col-md-6">
                <label class="form-control-label">{{trans('labels.front.First name')}} * :</label><br>
                <input type="text" id="fname" name="first_name" placeholder=""
                       class="form-control " value="{{old('first_name')}}" >
                <span class="text-danger">@error('first_name'){{ $message }} @enderror</span>
            </div>
            <div class="col-md-6">
                <label class="form-control-label">{{trans('labels.front.Last name')}} * :</label>
                <input type="text" id="lname" name="last_name" placeholder=""
                       class="form-control "  value="{{old('last_name')}}">
                <span class="text-danger">@error('last_name'){{ $message }} @enderror</span>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                <label class="form-control-label">{{trans('labels.front.E-MAIL ADDRESS')}} * :</label>
                <input type="email" id="email" name="email" value="{{old('email')}}" placeholder="" class="form-control "
                       >
                <span class="text-danger">@error('email'){{ $message }} @enderror</span>
            </div>
            <div class="col-md-6">
                <label class="form-control-label">{{trans('labels.front.Telephone number')}} * :</label>
                <input type="number" id="mob" name="telephone" placeholder=""
                       class="form-control "  value="{{old('telephone')}}">
                <span class="text-danger">@error('telephone'){{ $message }} @enderror</span>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-6">
                <label class="form-control-label">{{trans('labels.front.position')}} * :</label>
                <input type="text" id="position" name="position" placeholder=""
                       class="form-control "  value="{{old('position')}}">
                <span class="text-danger">@error('position'){{ $message }} @enderror</span>
            </div>
            <div class="col-md-6">
                <label class="form-control-label">{{trans('labels.front.side')}} * :</label>
                <input type="text" id="side" name="side" placeholder=""
                       class="form-control "  value="{{old('side')}}">
                <span class="text-danger">@error('side'){{ $message }} @enderror</span>
            </div>

        </div>
        <div class="row form-group">

            <div class="col-md-12">
                <label class="form-control-label">{{trans('labels.front.Questions you want to ask')}} * :</label>
                <textarea id="message" rows="5" value="{{old('message')}}" name="message" class="form-control" style="height: auto;"></textarea>
                <span class="text-danger">@error('message'){{ $message }} @enderror</span>
            </div>

        </div>
        <div class="row form-group">

            <div class="col-md-12">
                <button type="submit" id="submit_btn" class=" btn btn-primary mt-3 mb-1 next">
                    <span style="vertical-align: middle;">{{trans('buttons.front.Registration')}}</span>
                    <span class="fa fa-long-arrow-right"></span>
                </button>
            </div>
        </div>
    </div>
</fieldset>
