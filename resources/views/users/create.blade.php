@extends('layouts.app', ['activePage' => 'admin-management', 'titlePage' => __('Admin Management')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Admin</h4>
                            <p class="card-category"> add new admin</p>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{route('user.store')}}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="first_name">First Name</label>
                                        <input type="text" class="form-control" name="first_name" id="first_name" required placeholder="First Name ..." value="{{old('first_name')}}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" class="form-control" name="last_name" id="last_name" required placeholder="Last Name ..." value="{{old('last_name')}}">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" required placeholder="Email ..." value="{{old('email')}}">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="password">Password</label>
                                        <input type="password" min="8" class="form-control" name="password" id="password" placeholder="********">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="password_confirmation">Confirm Password</label>
                                        <input type="password" min="8" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="********">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="role">Role</label>
                                        <select class="form-control selectpicker" name="role" required>
                                            <option selected disabled>Select a Role</option>
                                            @foreach($roles as $role)
                                                <option value="{{$role->name}}" @if(old('role') == $role->name) selected @endif>{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary float-right">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

@endpush
