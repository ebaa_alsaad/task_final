@extends('layouts.app', ['activePage' => 'admin-management', 'titlePage' => __('Admin Management')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{$message}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Admin</h4>
                            <p class="card-category"> Here you can manage admins</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{route('user.create')}}" class="btn btn-sm btn-primary">Add Admin</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="main-table">
                                    <thead class="text-primary">
                                    <tr>
                                        <th>ID</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        var table;
        $(function () {
            table = $('#main-table').DataTable({
                fixedHeader:true,
                processing: true,
                serverSide: true,
                lengthMenu : [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                ajax: '{!! route('user.index') !!}',
                dom: 'QBlfrtip',
                buttons: [

                ],
                columns: [
                    {"data": "id"},
                    {"data": "first_name"},
                    {"data": "last_name"},
                    {"data": "email"},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });
            table.on('click', '.delete', function () {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent')
                }
                var data = table.row($tr).data();
                deleteOpr(data.id, `admin/user/` + data.id, table);
            });
        });
    </script>
@endpush
