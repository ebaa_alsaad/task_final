@extends('layouts.app', ['activePage' => 'admin-management', 'titlePage' => __('Admin Management')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Admin</h4>
                            <p class="card-category"> edit admin</p>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{route('user.update',$model->id)}}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="first_name">First Name</label>
                                        <input type="text" class="form-control" name="first_name" id="first_name" required value="{{old("first_name")??$model->first_name}}" placeholder="First Name ...">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" class="form-control" name="last_name" id="last_name" required value="{{old("last_name")??$model->last_name}}" placeholder="Last Name ...">
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" required value="{{old("email")??$model->email}}" placeholder="Email ...">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="password">Password</label>
                                        <input type="password" min="8" class="form-control" name="password" id="password" placeholder="********">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="password_confirmation">Confirm Password</label>
                                        <input type="password" min="8" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="********">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="role">Role</label>
                                        <select class="form-control selectpicker" name="role" required>
                                            <option selected disabled>Select a Role</option>
                                            @foreach($roles as $role)
                                                <option @if($user_role == $role->name || old("role") == $role->name) selected @endif value="{{$role->name}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary float-right">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

@endpush
