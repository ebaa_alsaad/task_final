{{--<link rel="apple-touch-icon" sizes="31x31" href="{{asset('favicon.png')}}" type="image/png">--}}
{{--<link id="favicon" rel="shortcut icon" type="image/png" href="{{asset('favicon.png')}}">--}}
<!--     Fonts and icons     -->
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link href="{{ asset('material/css/style.css') }}" rel="stylesheet" />
