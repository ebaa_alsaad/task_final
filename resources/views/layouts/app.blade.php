<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <title>{{ __('Task') }}</title>
{{--    <link rel="apple-touch-icon" sizes="31x31" href="{{asset('favicon.png')}}" type="image/png">--}}
{{--    <link id="favicon" rel="shortcut icon" type="image/png" href="{{asset('favicon.png')}}">--}}
    @include('layouts.styles')
    </head>
    <body class="{{ $class ?? '' }}">
        @auth()
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @include('layouts.page_templates.auth')
        @endauth
        @guest()
            @include('layouts.page_templates.guest')
        @endguest

        @include('layouts.scripts')

        @stack('scripts')
    </body>
</html>
