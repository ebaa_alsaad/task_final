<!--   Core JS Files   -->
<script>
    var BASE_URL = '{{url('')}}' + '/';
    var _token = '{{csrf_token()}}';
</script>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/general.js')}}"></script>

<script src="{{ asset('material/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
<script src="{{ asset('material/js/material-dashboard.js?v=2.1.1') }}" type="text/javascript"></script>

<script src="https://cdn.datatables.net/colreorder/1.5.6/js/dataTables.colReorder.min.js"></script>
