<!-- Theme Base, Components and Settings -->
<script>
    var BASE_URL = '{{url('')}}' + '/';
    var _token = '{{csrf_token()}}';
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>--}}
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/general.js')}}"></script>
<script src="{{asset('js/form.js')}}"></script>
<script src="{{asset('js/index.js')}}"></script>

@stack('footer-scripts')


