<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">

    <title>Index</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    @include('layouts.front.includes.links')

</head>

<body>

<!-- ============================================================== -->

@yield('content')

<!-- End of Content Wrapper -->

<!-- Footer -->
@include('layouts.front.includes.footer')
<!-- End of Footer -->

<!-- ============================================================== -->

@include('layouts.front.includes.scripts')

</body>

</html>
