<div class="sidebar" data-color="orange" data-background-color="white"
     data-image="{{ asset('material') }}/img/sidebar-1.jpg">
    <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo">
        <a href="{{route('dashboard')}}" class="simple-text logo-normal">
            Task
            <br>
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="material-icons">dashboard</i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            <ul class="nav">
                <li class="nav-item{{ $activePage == 'admin-management' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('user.index') }}">
{{--                        <span class="sidebar-mini"> UM </span>--}}
                        <span class="sidebar-normal"> {{ __('Admin Management') }} </span>
                    </a>
                </li>
            </ul>
            <ul class="nav">
                <li class="nav-item{{ $activePage == 'Invited-people-management' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('Invited.index') }}">
{{--                        <span class="sidebar-mini"> CM </span>--}}
                        <span class="sidebar-normal"> {{ __('Invited People Management') }} </span>
                    </a>
                </li>
            </ul>
            <ul class="nav">
                <li class="nav-item{{ $activePage == 'Registered-people-management' ? ' active' : '' }}">
                    <a class="nav-link" href="{{ route('clients.index') }}">
{{--                        <span class="sidebar-mini"> AM </span>--}}
                        <span class="sidebar-normal"> {{ __('Registered People Management') }} </span>
                    </a>
                </li>
            </ul>


        </ul>
    </div>
</div>
