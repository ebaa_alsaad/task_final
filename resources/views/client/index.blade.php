@extends('layouts.app', ['activePage' => 'Registered-people-management', 'titlePage' => __('Registered People Management')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{$message}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Registered People</h4>
                            <p class="card-category"> Here you can manage Data </p>
                        </div>
                        <div class="card-body">

                            <div class="table-responsive">
                                <table class="table" id="main-table">
                                    <thead class="text-primary">
                                    <tr>
{{--                                        <th>ID</th>--}}
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Telephone</th>
                                        <th>Position</th>
                                        <th>Company</th>
                                        <th>Seat Number</th>
                                        <th>Message</th>
                                        <th>Attended</th>
                                        <th>Registered at</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        var table;
        $(function () {
            table = $('#main-table').DataTable({
                fixedHeader:true,
                processing: true,
                serverSide: true,
                colReorder: true,
                // scrollX: true,
                lengthMenu : [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                ajax: '{!! route('clients.index') !!}',
                dom: 'QBlfrtip',
                buttons: [
                    'csv',
                ],
                columns: [
                    {"data": "first_name"},
                    {"data": "last_name"},
                    {"data": "email"},
                    {"data": "telephone"},
                    {"data": "position"},
                    {"data": "side"},
                    {"data": "seat_number"},
                    {"data": "message"},
                    {"data": "is_attended" , "render" : function (data){
                            if(data === 0)
                                return "No";
                            if(data === 1)
                                return "Yes";
                            return "--"
                        }},

                    {"data": "created_at"},

                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });
            table.on('click', '.delete', function () {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent')
                }
                var data = table.row($tr).data();
                deleteOpr(data.id, `admin/clients/` + data.id, table);
            });
        });
    </script>
@endpush
