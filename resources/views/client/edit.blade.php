@extends('layouts.app', ['activePage' => 'Registered-people-management', 'titlePage' => __('Registered People Management')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Registered Person</h4>
                            <p class="card-category"> edit data</p>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{route('clients.update',$model->id)}}">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="first_name">First Name</label>
                                        <input type="text" class="form-control" name="first_name" id="first_name" required value="{{$model->first_name}}" placeholder="First Name ...">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" class="form-control" name="last_name" id="last_name" required value="{{$model->last_name}}" placeholder="Last Name ...">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" required value="{{$model->email}}" placeholder="Email ...">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="telephone">Telephone</label>
                                        <input type="tel" class="form-control" name="telephone" id="telephone" required value="{{$model->telephone}}" placeholder="Email ...">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="Position">Position</label>
                                        <input type="text" class="form-control" name="position" id="Position" value="{{$model->position}}"  placeholder="position">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="side">Company</label>
                                        <input type="text" class="form-control" name="side" id="side" value="{{$model->side}}"  placeholder="side">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="seat_number">Seat Number</label>
                                        <input type="text" class="form-control" name="seat_number" id="seat_number" value="{{$model->seat_number}}"  placeholder="Seat Number">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="is_attended">Attended</label>
                                        <select name="is_attended" id="is_attended"  class="form-control selectpicker"  required>
                                            <option value="0" @if($model->is_attended == 0) selected @endif >No</option>
                                            <option value="1" @if($model->is_attended == 1) selected @endif >Yes</option>
                                        </select>
                                    </div>


                                    <div class="form-group col-md-12">
                                        <label for="message">Message</label>
                                        <input type="text" class="form-control" name="message" id="message" value="{{$model->message}}"  placeholder="Message">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary float-right">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

@endpush
