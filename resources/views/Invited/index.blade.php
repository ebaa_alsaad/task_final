@extends('layouts.app', ['activePage' => 'Invited-people-management', 'titlePage' => __('Invited People Management')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{$message}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Invited People</h4>
                            <p class="card-category"> Here you can manage Data</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 text-right">
                                    <a href="{{route('Invited.create')}}" class="btn btn-sm btn-primary">Add invited person mail</a>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table" id="main-table">
                                    <thead class="text-primary">
                                    <tr>

                                        <th>Client Email</th>
                                        <th>Registration at</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        var table;
        $(function () {
            table = $('#main-table').DataTable({
                fixedHeader:true,
                processing: true,
                serverSide: true,
                colReorder: true,
                // scrollX: true,
                lengthMenu : [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
                ajax: '{!! route('Invited.index') !!}',
                dom: 'QBlfrtip',
                buttons: [
                ],
                columns: [

                    {"data": "email"},

                    {"data": "registered_at"},

                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });
            table.on('click', '.delete', function () {
                $tr = $(this).closest('tr');
                if ($($tr).hasClass('child')) {
                    $tr = $tr.prev('.parent')
                }
                var data = table.row($tr).data();
                deleteOpr(data.id, `admin/Invited/` + data.id, table);
            });
        });
    </script>
@endpush
