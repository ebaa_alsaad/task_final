@extends('layouts.app', ['activePage' => 'Invited-people-management', 'titlePage' => __('Invited People Management')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Invited People</h4>
                            <p class="card-category">add new data</p>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{route('Invited.store')}}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="email">E-mail Of Person</label>
                                        <input type="email" class="form-control" name="email" id="email">
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary float-right">Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

@endpush
