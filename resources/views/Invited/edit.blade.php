@extends('layouts.app', ['activePage' => 'Invited-people-management', 'titlePage' => __('Invited People Management')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Invited People</h4>
                            <p class="card-category"> Edit Data</p>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{route('Invited.update',$model->id)}}">
                                @csrf
                                @method('PUT')
                                <div class="row">

                                    <div class="form-group col-md-6">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" required value="{{$model->email}}" placeholder="Email ...">
                                    </div>

                                </div>
                                <button type="submit" class="btn btn-primary float-right">Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')

@endpush
