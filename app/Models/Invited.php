<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invited extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $casts = [
        'created_at' => 'datetime:Y-m-d / H:i',
        'registered_at' => 'datetime:Y-m-d / H:i',
    ];
}
