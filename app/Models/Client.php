<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Class Client
 * @mixin Builder
 * @package App\Models
 */
class Client extends Model
{
    use HasFactory,Notifiable;

    protected $guarded = ['id'];
    protected $casts = [
        'created_at' => 'datetime:Y-m-d H:i',
    ];

    public function data_forms(){
        return $this->hasOne(DataForm::class,'client_id');
    }
}
