<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param Request $request
     * @param DataTables $dataTables
     * @return JsonResponse|Application|Factory|View
     * @throws Exception
     */
    public function index(Request $request , DataTables $dataTables){
        $items = User::with(['something'])->select('users.*');
        if($request->ajax()){
            return $dataTables->eloquent($items)
                ->addColumn('action', function ($item) {
                return '
                        <div style="white-space: nowrap;" class="td-actions text-right">
                            <a class="edit btn btn-xs btn-primary" style="color:#fff" href="'.route('user.edit',$item->id).'" ><i class="fa fa-edit"></i></a>
                            <a class="delete btn btn-xs btn-dark" style="color:#fff"><i class="fa fa-trash"></i></a>
                        </div>';
                })
                ->make(true);
        }
        return view('users.index');
    }

    public function create(){
        $roles = Role::all();
        return view('users.create',compact('roles'));
    }

    public function store(Request $request){
        $validated = $request->validate([
            'first_name' => ['required', 'min:3','string'],
            'last_name' => ['required', 'min:3','string'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required','confirmed','min:8'],
            'role' => ['required','string', Rule::in([RoleEnum::ADMIN])],
        ]);
        $role = $validated['role'];
        unset($validated['role']);

        $user = User::create($validated);
        $user->assignRole($role);

        return redirect()->route('user.index')->with('success','User Added Successfully');
    }

    public function update(Request $request,$id){
        $validated = $request->validate([
            'first_name' => ['required', 'min:3','string'],
            'last_name' => ['required', 'min:3','string'],
            'email' => ['required', 'email', 'unique:users,email,'.$id],
            'password' => ['nullable','confirmed','min:8'],
            'role' => ['required','string', Rule::in([RoleEnum::ADMIN])],
        ]);

        $role = $validated['role'];
        unset($validated['role']);

        $model = User::findOrFail($id);
        $model->update($validated);
        $model->assignRole($role);

        return redirect()->route('user.index')->with('success','User Updated Successfully');
    }

    public function edit($id){
        $model = User::findOrFail($id);
        $roles = Role::all();
        $user_role = optional($model->roles()->first())->name;
        return view('users.edit',compact('model','roles','user_role'));
    }

    public function destroy($id){
        $model = User::findOrFail($id);
        $model->delete();
        return response()->json(['message' => 'Successfully Deleted!']);
    }
}
