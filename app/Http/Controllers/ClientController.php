<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;

use App\Models\Client;

use App\Models\Invited;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Yajra\DataTables\DataTables;

class ClientController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param Request $request
     * @param DataTables $dataTables
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|JsonResponse
     * @throws \Exception
     */
    public function index(Request $request , DataTables $dataTables){
        $items = Client::select('clients.*');

        if($request->ajax()){
            return $dataTables->eloquent($items)
                ->addColumn('action', function ($item) {

                    return '
                            <div style="white-space: nowrap;" class="td-actions text-right">
                                <a class="btn btn-xs btn-warning" title="download Qrcode" style="color:#fff" download  href="'. asset('/'.$item->qrcode) .'"><i class="fa fa-qrcode"></i></a>
                                <a class="edit btn btn-xs btn-primary" style="color:#fff" href="'.route('clients.edit',$item->id).'" ><i class="fa fa-edit"></i></a>
                                <a class="delete btn btn-xs btn-dark" style="color:#fff"><i class="fa fa-trash"></i></a>
                            </div>';
                })
                ->make(true);
        }
        return view('client.index');
    }

    public function create(){
        return view('client.create');
    }

    public function store(Request $request){
        $model = Client::create($request->all());
        return redirect()->route('clients.index')->with('success','Added Successfully');
    }

//    public function show($data_form_id){
//        $model = DataForm::with(['client'])->find($data_form_id);
//        $client = $model->client;
//        return view('clients.show',compact(['model','client']));
//    }

    public function register(){
        return view('front.index');
    }
    public function save_data(Request $request){
        $validated =$request->validate([
            'first_name' => ['required', 'string', 'max:45'],
            'last_name'  => ['required', 'string', 'max:45'],
            'email'      => ['required', 'string', 'email', 'max:45'],
            'telephone'  => ['required', 'string', 'max:45'],
            'position'   => ['required', 'string', 'max:45'],
            'side'       => ['required', 'string', 'max:45'],
            'message'    => ['required', 'string'],
        ]);

        $invitation = Invited::where('email',$validated['email'])->first();
        $message = "You Have Been Registered";
        if($invitation){
            $invitation->registered_at = $invitation->registered_at ?? now();
            $invitation->save();
            $message = "Your Invitation has been Confirmed!";
        }

        $old_client = Client::where('email',$validated['email'])->first();

        $random_str = Uuid::uuid4()->toString();

        QrCode::size(400)->generate($random_str , public_path('img/qr/'.$random_str . '.svg'));

        $seat_number = sprintf('%08d', rand(111111,999999));
        do{
            $seat_number = sprintf('%08d', rand(111111,999999));
        }while(Client::where('seat_number',$seat_number)->count() > 0);

        if($old_client){
            $old_client->update($validated + ['qrcode' => 'img/qr/'.$random_str . '.svg','seat_number'=>$seat_number]);
        } else {
            $client = Client::create($validated + ['qrcode' => 'img/qr/'.$random_str . '.svg','seat_number'=>$seat_number]);
        }


        return response()->json(["message" => $message]);
    }

    public function update(Request $request,$id){
        $model = Client::findOrFail($id);
        $model->update($request->all());
        return redirect()->route('clients.index')->with('success','Updated Successfully');
    }

    public function edit($id){
        $model = Client::findOrFail($id);
        return view('client.edit',compact(['model']));
    }

    public function destroy($id){
        $model = Client::findOrFail($id);;
        $model->delete();
        return response()->json(['message' => 'Successfully Deleted!']);
    }


}
