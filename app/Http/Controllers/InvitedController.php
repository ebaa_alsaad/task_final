<?php

namespace App\Http\Controllers;

use App\Models\Invited;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class InvitedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request , DataTables $dataTables){
        $items = Invited::select('inviteds.*');

        if($request->ajax()){
            return $dataTables->eloquent($items)
                ->addColumn('action', function ($item) {

                    return '
                            <div style="white-space: nowrap;" class="td-actions text-right">
                                <a class="edit btn btn-xs btn-primary" style="color:#fff" href="'.route('Invited.edit',$item->id).'" ><i class="fa fa-edit"></i></a>
                                <a class="delete btn btn-xs btn-dark" style="color:#fff"><i class="fa fa-trash"></i></a>
                            </div>';
                })
                ->make(true);
        }
        return view('Invited.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('Invited.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $model = Invited::create($request->all());
        return redirect()->route('Invited.index')->with('success','Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invited  $invited
     * @return \Illuminate\Http\Response
     */
    public function show(Invited $invited)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invited  $invited
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $model = Invited::findOrFail($id);
        return view('Invited.edit',compact(['model']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invited  $invited
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id){
        $model = Invited::findOrFail($id);
        $model->update($request->all());
        return redirect()->route('Invited.index')->with('success','Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invited  $invited
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $model = Invited::findOrFail($id);;
        $model->delete();
        return response()->json(['message' => 'Successfully Deleted!']);
    }
}
