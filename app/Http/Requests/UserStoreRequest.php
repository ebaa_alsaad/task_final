<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required', 'min:3','string'],
            'last_name' => ['required', 'min:3','string'],
            'email' => ['required', 'email', Rule::unique((new User)->getTable(),'email')],
            'password' => ['required','confirmed','min:8'],
        ];
    }
}
