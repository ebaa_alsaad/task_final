// from - http://cssdeck.com/labs/ql8jmgjt

function updateLog() {
  var one = $("#opt_1:checked").val() ? "On" : "Off"
  var two = $("#opt_2:checked").val() ? "On" : "Off"
  $(".log").html("Everyone: " + one + "<br/>Just for me: " + two)
}

$(".radio-group__option").change(updateLog)


/*//// slider range ////*/

var rangeSlider = document.getElementById("rs-range-line");
var rangeBullet = document.getElementById("rs-bullet");

// rangeSlider.addEventListener("input", showSliderValue, false);

function showSliderValue() {
    rangeBullet.innerHTML = rangeSlider.value;
    var bulletPosition = (rangeSlider.value /rangeSlider.max);
    rangeBullet.style.left = (bulletPosition * 578) + "px";
}
