function showAlert(options = null, onConfirm, onCancel) {
    let defaultOptions = {
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
    };
    let $options = $.extend(defaultOptions, options);
    new window.swal($options).then(function (result) {
        if (result) {
            if (result.isConfirmed)
                onConfirm();
        }
    }).catch(window.swal.noop);
}

HandleJsonErrors = function (xhr, errorThrown) {
    switch (xhr.status) {
        case 400:
            if (typeof xhr.responseJSON.message === 'string') {
                window.toastr.error(xhr.responseJSON.message, 400);
            }
            if (typeof xhr.responseJSON.message === 'object') {
                _message = '<div class="text-left">';
                for (let error in xhr.responseJSON.message) {
                    _message += xhr.responseJSON.message[error];
                    let input;
                    if (error && error.includes('.')) {
                        let $error = error.replace('.', '[') + ']';
                        input = $("[name='" + $error + "']");

                    } else
                        input = $('[name=' + error + ']');
                    let errorCont = input.closest('span.error');
                    if (errorCont.length) {
                        errorCont.text(xhr.responseJSON.message[error][0]);
                    } else {
                        if (input.hasClass('select2')) {
                            input.parent().find('.select2-container').after('<span class="text-danger error">' + xhr.responseJSON.message[error][0] + '</span>');
                        } else
                            input.after('<span class="text-danger error">' + xhr.responseJSON.message[error][0] + '</span>');
                    }
                }
                _message += '</div>';

                window.toastr.error(_message, '400', {
                    timeOut: 3000
                });
            }
            break;
        case 401:
            window.toastr.warning(xhr.responseJSON.message, '401');
            if (window.location.pathname !== '/login') {
                setTimeout(function () {
                    window.location = getBaseURL() + 'login';
                }, 1500);
            }
            break;
        case 404:
            window.toastr.error(xhr.responseJSON.message, '404');
            break;
        case 422:
            _message = '<div class="text-left">';
            for (let error in xhr.responseJSON.errors) {
                _message += xhr.responseJSON.errors[error][0];
                let input;
                if (error && error.includes('.')) {
                    let $error = error.replace('.', '[') + ']';
                    input = $("[name='" + $error + "']");
                } else {
                    input = $('[name=' + error + ']');
                }
                let errorCont = input.closest('span.error');
                if (errorCont.length) {
                    errorCont.text(xhr.responseJSON.errors[error][0]);
                } else {
                    if (input.hasClass('select2')) {
                        input.parent().find('.select2-container').after('<span class="text-danger error">' + xhr.responseJSON.errors[error][0] + '</span>');
                    } else if (input.parent().hasClass('input-group')) {
                        input.parent().after('<span class="text-danger error">' + xhr.responseJSON.errors[error][0] + '</span>');
                    } else
                        input.after('<span class="text-danger error">' + xhr.responseJSON.errors[error][0] + '</span>');
                }
            }
            _message += '</div>';
            window.toastr.error(_message, '422', {
                timeOut: 5000
            });
            break;
        case 500:
            window.toastr.error(xhr.responseJSON.message, '500');
            break;
        default:
            window.toastr.error('Something went wrong: ' + errorThrown, 'Oops..');
    }
};



function deleteOpr(id, $url,table, $options = null) {
    showAlert($options, () => {
        $.ajax({
            type: 'POST',
            url: BASE_URL + $url,
            data: {_method: 'DELETE', _token},
            success(xhr) {
                // window.toastr.success(xhr.message);
                new window.swal(
                    'success',
                    xhr.message,
                    'success'
                );
                table.ajax.reload();
                // reloadDataGrid(true);
                return true;
            },
            error: HandleJsonErrors
        });
        return false;
    },()=>{
        table.ajax.reload();
    });
}
