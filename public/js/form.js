function validate1(val) {
    v1 = document.getElementById("fname");
    v2 = document.getElementById("lname");
    v3 = document.getElementById("email");
    v4 = document.getElementById("mob");
    v5 = document.getElementById("postcode");
    v6 = document.getElementById("straat");
    v7 = document.getElementById("Nr");

    flag1 = true;
    flag2 = true;
    flag3 = true;
    flag4 = true;
    flag5 = true;
    flag6 = true;
    flag7 = true;

    if(val>=1 || val==0) {
        if(v1.value == "") {
            v1.style.borderColor = "red";
            flag1 = false;
        }
        else {
            v1.style.borderColor = "green";
            flag1 = true;
        }
    }

    if(val>=2 || val==0) {
        if(v2.value == "") {
            v2.style.borderColor = "red";
            flag2 = false;
        }
        else {
            v2.style.borderColor = "green";
            flag2 = true;
        }
    }

    if(val>=3 || val==0) {
        if(v3.value == "") {
            v3.style.borderColor = "red";
            flag3 = false;
        }
        else {
            v3.style.borderColor = "green";
            flag3 = true;
        }
    }

    if(val>=4 || val==0) {
        if(v4.value == "") {
            v4.style.borderColor = "red";
            flag4 = false;
        }
        else {
            v4.style.borderColor = "green";
            flag4 = true;
        }
    }
    if(val>=5 || val==0) {
        if(v5.value == "") {
            v5.style.borderColor = "red";
            flag5 = false;
        }
        else {
            v5.style.borderColor = "green";
            flag5 = true;
        }
    }
    if(val>=6 || val==0) {
        if(v6.value == "") {
            v6.style.borderColor = "red";
            flag6 = false;
        }
        else {
            v6.style.borderColor = "green";
            flag6 = true;
        }
    }
    if(val>=7 || val==0) {
        if(v7.value == "") {
            v7.style.borderColor = "red";
            flag7 = false;
        }
        else {
            v7.style.borderColor = "green";
            flag7 = true;
        }
    }

    flag = flag1 && flag2 && flag3 && flag4 && flag5 && flag6 && flag7;

    return flag;
}

// $(".validate_form").validate({
//     rules: {
//         email: {
//             required: true,
//             email: true,
//             remote: "http://localhost:3000/inputValidator"
//         },
//         password: {
//             required: true,
//             strongPassword: true
//         },
//         password2: {
//             required: true,
//             equalTo: '#password'
//         },
//         firstName: {
//             required: true,
//             nowhitespace: true,
//             lettersonly: true
//         },
//         secondName: {
//             required: true,
//             nowhitespace: true,
//             lettersonly: true
//         },
//         businessName: {
//             required: true
//         },
//         phone: {
//             required: true,
//             digits: true,
//             phonesUK: true
//         },
//         address: {
//             required: true
//         },
//         town: {
//             required: true,
//             lettersonly: true
//         },
//         postcode: {
//             required: true,
//             postcodeUK: true
//         },
//         terms: {
//             required: true
//         }
//     },
//     messages: {
//         email: {
//             required: 'Please enter an email address.',
//             email: 'Please enter a <em>valid</em> email address.',
//             remote: $.validator.format("{0} is already associated with an account.")
//         }
//     }
// });


$(document).ready(function(){
    // Get value on button click
    $("#opt_2_status").click(function(){
        var str = $("#opt_2_status").val();
        if(str == '1'){
            // $('.hide_sec').hide();
            $('.opt_2').css('background', '#f5a625c7');
            $('.opt_2').css('color', 'white');
            $('.opt_1').css('background', 'linear-gradient(60deg, #bbabab7a, #bbabab99)');
            $('.opt_1').css('color', '#ffffff');
            $('.offer').css('cursor', 'default'); // 'default'
        }
    });
    $("#opt_1_status").click(function(){
        var str = $("#opt_1_status").val();
        if(str == '0'){
            // $('.hide_sec').show();
            $('.opt_1').css('background', '#f5a625c7');
            $('.opt_1').css('color', 'white');
            $('.opt_2').css('background', 'linear-gradient(60deg, #bbabab7a, #bbabab99)');
            $('.opt_2').css('color', '#ffffff');
        }
    });
    $('.offer_val').on('input',function(event)
    {
        event.preventDefault();
        if( $(this).val() > 0 ) {
            $('.offer').removeAttr('disabled'); //enable
            $('.offer').css('cursor', 'pointer'); // 'default'
        }
        else{
            $('.offer').attr('disabled','disabled'); //disable
            $('.offer').css('cursor', 'default'); // 'default'
        }
    });

});

// $(document).ready(function(){
//     // Get value on button click
//         if ($( ".depth_roof_clik" ).hasClass("selected")) {
//             $('.length_sloping').hide();
//             $('.depth_roof').show();
//         }else (!$(".depth_roof_clik").hasClass("selected"))
//         {
//             $('.length_sloping').show();
//             $('.depth_roof').hide();
//         }
// });


